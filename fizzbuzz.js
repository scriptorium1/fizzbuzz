for (let compteur = 1; compteur <= 100; compteur++) {
    FizzBuzz(compteur);
}

// Bonus 1
function FizzBuzz(compteur) {
    // définir réponse particulière pour multiple de 3 et 5 sinon conditions ne fonctionnent pas correctement
    if (compteur % 3 === 0 && compteur % 5 === 0) {
        console.log(compteur, "FizzBuzz");
    } else if (compteur % 3 === 0)
    // définir réponse particulière pour multiple de 3 
    {
        console.log(compteur, "Fizz");
    } else if (compteur % 5 === 0)
    // définir réponse particulière pour multiple de 5
    {
        console.log(compteur, "Buzz");
    } else {
        console.log(compteur);
    }
}

/* Bonus 2 : à remplacer au niveau des lignes 10 pour 3 et 14 pour 5 
et aussi au niveau de la ligne 8 pour les 2
à la place de compteur sous forme else if (dividedBy3ou5(compteur))
*/
function dividedBy3(number) {
    if (number % 3 === 0) {
        return true;
    } else {
        return false;
    }
}

function dividedBy5(number) {
    if (number % 5 === 0) {
        return true;
    } else {
        return false;
    }
}